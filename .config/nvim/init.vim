call plug#begin(expand('~/.config/nvim/plugged'))
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'airblade/vim-gitgutter'
" Plug 'vim-python/python-syntax'
Plug 'morhetz/gruvbox'
call plug#end()

" sets the colorscheme
colorscheme gruvbox

" sets background as transparent
hi Normal guibg=NONE ctermbg=NONE 

" indentation settings
set autoindent
filetype indent on

" line wrap
set wrap

" line numbers
set number

" Speed up scrolling in Vim
set ttyfast

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" enables syntax
syntax enable
set tabstop=4
set shiftwidth=4
set expandtab
set encoding=utf-8
set fileformat=unix

" Makes Cursor changes shape (block shape in normal mode and line in insert
" mode)
let &t_SI="\e[6 q"
let &t_EI="\e[2 q"
"*****************************************************************************
"" Abbreviations
"*****************************************************************************
"" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
cnoreabbrev qw wq
cnoreabbrev aq qa 
cnoreabbrev X x
cnoreabbrev ff FZF

" enables copying and pasting from and to the system clipboard
vnoremap <C-c> "+y
map <C-v> "+p
