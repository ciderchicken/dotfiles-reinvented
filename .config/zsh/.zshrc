#  ____          _            ____ _                         
# / ___|___   __| | ___ _ __ / ___| |__   ___  ___  ___  ___ 
#| |   / _ \ / _` |/ _ \ '__| |   | '_ \ / _ \/ _ \/ __|/ _ \
#| |__| (_) | (_| |  __/ |  | |___| | | |  __/  __/\__ \  __/
# \____\___/ \__,_|\___|_|   \____|_| |_|\___|\___||___/\___|
                                                            
### My zsh config ###

### EXPORT
export TERM="xterm-256color"                      # getting proper colors
export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"
export EDITOR="nvim"                              # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c"           # $VISUAL use Emacs in GUI mode

### SET MANPAGER "bat" as manpager
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export NO_AT_BRIDGE=1

### SET VI MODE ###
# Comment this line out to enable default emacs-like bindings
bindkey -v

# Fix backspace bug when switching modes
bindkey "^?" backward-delete-char

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

### PATH
if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/Applications" ] ;
  then PATH="$HOME/Applications:$PATH"
fi

### load the aliases ###
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"

### SETTING THE STARSHIP PROMPT ###
eval "$(starship init zsh)"

### fish-like syntax highlighting and autosuggestion
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Automatically cd into typed directory.
setopt autocd

### tab completions
autoload -U compinit
compinit
zstyle ':completion:*' menu select
# autocompletion with case incenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

### UwU
#figlet UwU UwU UwU | lolcat

### History
HISTFILE="$HOME/.cache/.zsh_history"
HISTSIZE=100000000000000000
SAVEHIST=$HISTSIZE

