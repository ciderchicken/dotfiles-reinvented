;; -*- lexical-binding: t -*-

;; makes emacs loadtime faster by reducing the garbage collector
(setq gc-cons-threshold (* 50 1000 1000))

;; turns off the default dashboard
(setq inhibit-startup-message t)

;; hides tool bar, menu bar and scroll bar
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; UTF-8 encoding things
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; tells emacs to NOT make backups and not to autosave stuff
(setq make-backup-files nil)
(setq auto-save-default nil)


;; turns yes or no questions to y or n
(defalias 'yes-or-no-p 'y-or-n-p)

;; Sets font to my preferred font
(add-to-list 'default-frame-alist
             '(font . "JetBrainsMono Nerd Font Bandit-10"))

;; sets melpa as the package archive
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;; Makes the dashboard buffer the default for when you use the emacs daemon
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))

;; golang syntax highlighting
(use-package go-mode)

;; icons for emacs
(use-package all-the-icons
  :ensure t)

;; sets up doom mode line bar (make sure to install all-the-icons pakage and then run M-x all-the-icons-install-fonts to get all the icons properly) 
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

;; sets up the superior dashboard package
(use-package dashboard
    :ensure t
    :config
    (dashboard-setup-startup-hook)
    (setq dashboard-startup-banner "~/.emacs.d/imgs/emacs_avatar.png")
    (setq dashboard-banner-logo-title "$ echo Windows is a failure")
    (setq dashboard-set-heading-icons t)
    (setq dashboard-set-file-icons t))

;; adds emoji support
(use-package emojify
  :hook (after-init . global-emojify-mode))

;; Magit
(use-package magit
  :ensure t
  :config
  (setq magit-push-always-verify nil)
  :bind
  ("C-c g" . magit-status))

;; Makes it so magit works with my dotfiles repo(git bare repo)
(defun ~/magit-process-environment (env)
  (let ((default (file-name-as-directory (expand-file-name default-directory)))
        (home (expand-file-name "~/")))
    (when (string= default home)
      (let ((gitdir (expand-file-name "~/.dotfiles-reinvented/"))) ;; bare repo directory
        (push (format "GIT_WORK_TREE=/home/void" home) env) ;; bare repo's work tree
        (push (format "GIT_DIR=/home/void/.dotfiles-reinvented" gitdir) env)))) ;; absolute path to the git bare repositort
  env)

(advice-add 'magit-process-environment
            :filter-return #'~/magit-process-environment)

;; vim emulation in emacs
(use-package evil)
(evil-mode 1)

;; line numbers 
(global-display-line-numbers-mode)

;; disable line numbers for some modes
(dolist (mode '(;;org-mode-hook
                term-mode-hook
		vterm-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; org mode improvements
(setq org-ellipsis " ")
(setq org-src-fontify-natively t)
(setq org-src-tab-acts-natively t)
(setq org-confirm-babel-evaluate nil)
(setq org-export-with-smart-quotes t)
(setq org-src-window-setup 'current-window)
(global-set-key (kbd "C-c a") #'org-agenda)
(setq org-startup-indented t) 

;; fancy org-mode bullets
(add-hook 'org-mode-hook 'org-indent-mode)(use-package org-bullets
  :ensure t
  :custom
  (org-bullets-bullet-list '("●" "●" "●" "●" "●" "●")) ;; custom bullet list because I don't like the default one C:
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;; always kill the current buffer
(defun kill-curr-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key (kbd "C-x k") 'kill-curr-buffer)

;; sets the theme
(use-package doom-themes
      :ensure t
      :config
      (load-theme 'doom-molokai t)
      (doom-themes-org-config)
      (doom-themes-visual-bell-config))
      
(setq ring-bell-function 'ignore)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files nil)
 '(package-selected-packages
   '(doom-modeline magit go-mode emojify org-bullets evil use-package doom-themes dashboard)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
